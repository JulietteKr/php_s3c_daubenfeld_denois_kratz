<?php

namespace mywishlist\controler;

use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueErreur;
use \mywishlist\vue\VueCompte;
use \mywishlist\models\Liste;
use \mywishlist\models\Compte;

class CompteControler{

  public function creerCompteControl(){
    $util = new \mywishlist\models\Compte();

    $login=$_POST['Login'];
    $mdp=$_POST['MotdePasse'];
    $hash=password_hash($mdp, PASSWORD_DEFAULT, ['cost'=> 12] );
    //$util = new \mywishlist\models\Compte($login, $hash);
    $util->login=$login;
    $util->mdp=$hash;
    $util->save();

    $this->afficherLesComptes();
  }

  public function authentifierCompte(){

    $login=$_POST['Login'];

    $util =\mywishlist\models\Compte::where("login","=",$login)->first();


    $mdp=$_POST['MotdePasse'];

    if (password_verify($mdp, $util->mdp)){
      echo "utilisateur connecté !";
      $_SESSION['user_id']= $util->id_compte;
      $liste = Liste::where('user_id','=',$_SESSION['user_id'])->get()->toArray();
      $vue = new VueCompte($liste);
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
    //$util = new \mywishlist\models\Compte($login, $hash);
  }

  public function formulaireConnexion(){

    $vueC = new VueCompte();
    $vueC->render(2);
  }

  public function afficherButtonsAccueil(){
    $vueC = new VueCompte();
    $vueC->render(0);
  }

  // public function deconnecter_utilisateur(){
  //   // if (isset($_GET['deconnexion_post_name'])){
  //   //   session_destroy();
  //   // }
  //   $vueC = new \mywishlist\vue\VueCompte();
  //   $vueC->render(0);
  // }

  public function afficher_Modifications(){
    $util = Compte::where("id_compte","=",$_SESSION['user_id'])->first()->toArray();
    $vue = new VueCompte($util);
    $vue->render(1);
  }

  public function post_Modifications(){
    
    if(($_POST['mdp_nouveau'] == $_POST['mdp_nouveau_confirmation'])){
      $vue = new VueCompte();
      $vue->render(4);
    }else{
      $vue = new VueErreur();
      $vue->render(1);
    }

  }

  public function afficherLesComptes(){
    $comptes = Compte::get()->toArray();
    $vueC = new VueCompte($comptes);
    $vueC->render(3);

  }






}
