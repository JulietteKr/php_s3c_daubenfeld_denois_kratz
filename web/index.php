<?php

use \mywishlist\models\Liste;
use \mywishlist\models\Item;
use \mywishlist\controler\ListParticipationControler;
use \mywishlist\controler\ListCreateControler;
use \mywishlist\controler\CompteControler;
use \mywishlist\controler\ItemControler;
use \mywishlist\controler\ImageControler;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

session_start();

$db = new DB();
$db->addConnection( [
 'driver' => 'mysql',
 'host' => 'localhost',
 'database' => 'daubenfe6u',
 'username' => 'daubenfe6u',
 'password' => '12345',
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();
//echo "coucou";


// $app->post('/',function(){
//   $c2 = new CompteControler();
//   $c2->formulaireConnexion();
//
// });

// $test = Liste::get();
// var_dump($test);

$app->get('/',function(){
  $c2 = new CompteControler();
  $c2->afficherButtonsAccueil();
});


$app->post('/',function(){
  if (isset($_POST['deconnexion_post_name'])){
    session_destroy();
  }
  $c2 = new CompteControler();
  $c2->afficherButtonsAccueil();
});

$app->get('/authentification',function(){
  $c = new CompteControler();
  $c->formulaireConnexion();
});

$app->post('/authentification',function(){
  $c = new CompteControler();
  $c->authentifierCompte();
});

$app->get('/hello/world',function(){
  echo "hello world !";
});


$app->post('/listes',function(){
  $c2 = new ListCreateControler();
  $c2->creerListe();

});

$app->get('/listes',function(){
  $c2 = new ListParticipationControler();
  $c2->afficherListeDesListes();
});

$app->get('/listesCreateur',function(){
  $c = new ListParticipationControler();
  $c->afficherListesDuCreateur();
});

$app->get('/listeCreateur/:idliste', function($idliste){
  $c = new ItemControler();
  $c->afficherItems_Avec_Button_Image($idliste);
});

$app->get('/liste/:idliste',function($idliste){
  $c = new ListParticipationControler();
  $c->afficherItemsDeLaListe($idliste);
});

$app->post('/liste/:idliste',function($idliste){
  $c = new ListCreateControler();
  $c->ajouterItem($idliste);
});

$app->get('/item/reserve',function(){
    $c = new ListParticipationControler();
    $c->afficherItemReserve();
});

$app->get('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->afficherItem($id);
});

$app->get('/item/:id/img/ajouter',function($iditem){
  $c = new ImageControler();
  $c->formulaire_ajout_image_on_item($iditem);
});

$app->post('/item/:id/img/ajouter',function($iditem){
  $c = new ImageControler();
  $c->afficher_image_ajout($iditem);
});

$app->get('/item/:id/img/modifier',function($iditem){
  $c = new ImageControler();
  $c->formulaire_modif_image_on_item($iditem);
});

$app->post('/item/:id/img/modifier',function($iditem){
  $c = new ImageControler();
  $c->afficher_image_modif($iditem);
});

$app->get('/item/modifier/:id',function($iditem){
  $c = new ItemControler();
  $c->formulaire_modif_item($iditem);
});

$app->post('/item/modifier/:id',function($iditem){
  $c = new ItemControler();
  $c->afficher_modif_item($iditem);
});

$app->post('/listeCreateur/:idliste', function($idliste){
  if(isset($_POST['supprimer_img_item'])){
    $c = new ImageControler();
    $c->afficher_image_supprimer($idliste);
  }
  else if(isset($_POST['ajouter_item_listeCrea'])){
    $c = new ItemControler();
    $c->ajouter_item_listeCreateur($idliste);
  }else if(isset($_POST['supprimer_item_listeCrea'])){
      $c = new ItemControler();
      $c->supprimer_item_listeCrea($idliste);
  }else{
    $v = new VueErreur();
    $v->render(0);
  }
});

$app->post('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->reserver($id);
});

$app->post('/item/suppr/:id',function($id){
    $c = new ListParticipationControler();
    $c->supprimerItemDeListe($id);
});

 $app->post('/compte', function(){
   $c = new CompteControler();
   $c->creerCompteControl();
 });

 $app->get('/inscription', function(){
   $c = new CompteControler();
   $c->afficherLesComptes();
 });

 $app->post('/listes/modif',function(){
   $c2 = new ListParticipationControler();
   $c2->modifierListe($_POST['numeroListe']);
 });

 $app->post('/listes/supp',function(){
   $c3 = new ListParticipationControler();
   $c3->supprimerListe($_POST['numeroListe']);
 });

$app->get('/parametres',function(){
  $c = new CompteControler();
  $c->afficher_Modifications();
});

$app->post('/parametres',function(){
  $c = new CompteControler();
  $c->post_Modifications();
});

$app->get('/deconnection',function(){
  $c = new CompteControler();
  $c->deconnecter_utilisateur();
});


$app->run();

// session_destroy();
