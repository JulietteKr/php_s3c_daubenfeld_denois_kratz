<?php
namespace mywishlist\vue;
use \mywishlist\models\Image;

class VueCreateur{

  private $tableau;

  public function __construct($tab=null){
    $this->tableau = $tab;
  }

  public function afficherListesDuCreateur(){

    $res='<a href ="../web/">Accueil</a><br>';
    $res.='<section>';

    foreach($this->tableau as $t){
      $res.= 'Titre de la liste = '.$t['titre'].'<br>Description de la liste = '.$t['description'].'<br>';
      $res .= '<input type="button" value="Voir liste '.$t['no'].'" name="submit" onclick= "window.location = \'listeCreateur/'.$t['no'].'\'"><br>------<br>';
    }
    $res.='</section>';
    $res .= '<form id="creerListe" method="POST" action="./listes">
    <fieldset>
      <legend>Creation d une nouvelle liste</legend>
      <label for="f1_name">Titre de la nouvelle liste : </label>
      <input type="text" id="f1_name" name="titreListe" required>
      <label for="f2_name">Description :</label>
      <input type="text" id="f2_name" name="descrListe" required>
      <label for="f3_name">Date d expiration :</label>
      <input type="date" id="f3_name" name="expiration" required>

        <button type="submit">Creer la liste</button></form>
    </fieldset>';


    return $res;
  }

  public function items_button_image(){
    $res='<a href ="../../web/">Accueil</a><br>';
    $res.='<a href ="../listesCreateur">Afficher ses listes</a><br>';
    $res.='<section>';

    foreach($this->tableau as $tab){

      $res.= '<br> item : '.$tab['nom'].'<br>
              Réservé par : '.$tab['participant'].'
              Message :'.$tab['message'].'<br>';

      $image;
      //nécessaire pour vérifier qu'un item a une image ou non
      $image = Image::where('item_id','=',$tab['id'])->first();

      $res.= '<input type="button" value="Modifier l\'item \''.$tab['nom'].'\'"
                       name="submit" onclick= "window.location = \'../item/modifier/'.$tab['id'].'\'"><br>';

      if( isset($image) ){
      $res .= '<input type="button" value="Modifier l\'image à l\'item \''.$tab['nom'].'\'"
                       name="submit" onclick= "window.location = \'../item/'.$tab['id'].'/img/modifier\'"><br>';
      $res.='<form id="supprimer_image" method="POST" action="./'.$tab['id'].'">
                  <button type="submit" name="supprimer" value="supprimer_image_item">Supprimer l\'image de l\'item \''.$tab['nom'].'\'</button>
            </form>';
      }else {

          $res .= '<input type="button" value="Ajouter une image à l\'item \''.$tab['nom'].'\'"
                           name="submit" onclick= "window.location = \'../item/'.$tab['id'].'/img/ajouter\'"><br>------<br>';

      }
    }
    $res.='</section>';

    return $res;
  }

  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->afficherListesDuCreateur();
    break;

    case 1 :
    $content = $this->items_button_image();
    break;
  }
$html =<<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img src="CSS/logo.PNG" alt="My Wishlist"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
                    <li><a href="item/reserve">Afficher les items</a></li>
						        <li><a href="listes">Afficher les listes</a></li>
                    <li><a href="listesCreateur">Afficher ses listes</a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
						        <li><a href="./">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
echo $html;
}


}
