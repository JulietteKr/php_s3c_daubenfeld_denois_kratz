<?php
namespace mywishlist\vue;

class VueImage{

  private $tableau;

  public function __construct($tab=null){
    $this->tableau = $tab;
  }

  public function afficher_image_on_item(){
    $res='';
    $res='<a href ="../web/">Accueil</a><br>';
    $res .= '<a href ="../../../listesCreateur">Afficher ses listes</a><br>';

    $res.= 'Item numéro '.$this->tableau[0]['id'].'
            <br> Nom de l\'item = '.$this->tableau[0]['nom'].'
            <br> Description de l\'item = '.$this->tableau[0]['descr'].'
            <br> Participant : '.$this->tableau[0]['participant'].'
            <br> <img src="https://webetu.iutnc.univ-lorraine.fr/www/daubenfe6u/PHPS3/rendu/php_s3c_daubenfeld_denois_kratz/img/'.$this->tableau[1]['url'].'" <br>';


    return $res;
  }


  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->afficher_image_on_item();
    break;
  }
$html =<<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img src="CSS/logo.PNG" alt="My Wishlist"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
						        <li><a href="listes">Afficher les listes</a></li>
                    <li><a href="listesCreateur">Afficher ses listes</a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
						        <li><a href="./">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
echo $html;
}


}
