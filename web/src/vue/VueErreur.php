<?php
namespace mywishlist\vue;

class VueErreur{

  private $text;

  public function __construct($t=null){
    $this->text = $t;
  }

  private function erreur_Connection(){
    $res='erreur, aucun utilisateur ne s\'est connecté';
    return $res;
  }

  private function erreur_modifications_mdp(){
    $res='erreur, les mots de passes ne correspondent pas';
    return $res;
  }

  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->erreur_Connection();
    break;

    case 1 :
    $content = $this->erreur_modifications_mdp();
    break;


  }
$html = <<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img src="CSS/logo.PNG" alt="My Wishlist"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
						        <li><a href="listes">Afficher les listes</a></li>
                    <li><a href="listesCreateur">Afficher ses listes</a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
						        <li><a href="./">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
  echo $html;
  }
}
