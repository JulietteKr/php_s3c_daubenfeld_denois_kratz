<?php
namespace mywishlist\vue;

class VueParticipant{

  private $tableau;

public function __construct($tab=null){
  $this->tableau = $tab;
}

private function afficherListeDesListes(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Numero de la liste : '.$t['no'].'<br>Titre de la liste : '.$t['titre'].'<br>Description de la liste : '.$t['description'].'<br>Date d expiration : '.$t['expiration'].'<br>';
    $res .= '<input type="button" value="Voir liste '.$t['no'].'" name="submit" onclick= "window.location = \'liste/'.$t['no'].'\'"><br>------<br>';
  }
  $res.='</section>';
  $res .= '<form id="creerListe" method="POST" action="./listes">
  <fieldset>
    <legend>Creation d une nouvelle liste</legend>
    <label for="f1_name">Titre de la nouvelle liste : </label>
    <input type="text" id="f1_name" name="titreListe" required>
    <label for="f2_name">Description :</label>
    <input type="text" id="f2_name" name="descrListe" required>
    <label for="f3_name">Date d expiration :</label>
    <input type="date" id="f3_name" name="expiration" required>
    <button type="submit">Creer la liste</button></form>
  </fieldset>';

  $res .= '<form id="modifierListe" method="POST" action="./listes/modif">
  <fieldset>
    <legend>Modification une liste</legend>
    <label for="f1_name">Numero de la liste a modifier : </label>
    <input type="number" id="f1_name" name="numeroListe" required>
    <label for="f2_name">Titre :</label>
    <input type="text" id="f2_name" name="titreListe">
    <label for="f3_name">Description :</label>
    <input type="text" id="f3_name" name="descrListe">
    <label for="f4_name">Date d expiration :</label>
    <input type="date" id="f4_name" name="expiration">
    <button type="submit">Modifier la liste</button></form>
  </fieldset>';

  $res .= '<form id="supprimerListe" method="POST" action="./listes/supp">
  <fieldset>
    <legend>Suppression d\'une liste</legend>
    <label for="f1_name">Numero de la liste a supprimer : </label>
    <input type="number" id="f1_name" name="numeroListe" required>
    <button type="submit">Supprimer la liste</button></form>
  </fieldset>';
  return $res;
}

private function afficherItemsDeListe(){
  $res='<section>';
  //tableau contient une liste
  $res.= $this->tableau[0]['titre'].'<br> Description : '.$this->tableau[0]['description'].'<br>';
  $items = \mywishlist\models\Item::where('liste_id','=',$this->tableau[0]['no'])->get();

  foreach($items as $item){
    $res.= '<br> item : '.$item->nom.'<br> Réservé par : '.$item->participant.'
    // Message :'.$item->message.'<br>';
  }
  $res.='</section>';

  return $res;
}

private function afficherItem(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Item numéro '.$t['id'].'<br> Nom de l\'item = '.$t['nom'].'<br> Description de l\'item = '.$t['descr'].'<br> Participant : '.$t['participant'].'<br>';


      $iditem = $t['id'];

      if($this->tableau[0]['participant'] == NULL) {
          $res .= '<form id="reserver1" method="POST" action="' . $iditem . '">
                 <input type="text" placeholder="<message>" name="message">';

          $res .= '<button type="submit" name="reserver" value="Reserver_reserve1">Réserver</button>
                 </form>';
      }
        $res .= '<form id="supprimer" method="POST" action="./../item/suppr/' . $iditem . '">
                 <button type="submit" name="supprimer" value="Supprimer_supprime">Supprimer</button>
                 </form>';
  }
  return $res.'</section>';
}

private function afficher_formulaire_ajout_image(){
  $res='<section>';
  $res.= 'Item numéro '.$this->tableau['id'].'
  <br> Nom de l\'item = '.$this->tableau['nom'].'
  <br> Description de l\'item = '.$this->tableau['descr'].'
  <br> Participant : '.$this->tableau['participant'].'<br>
  <br> Prix : '.$this->tableau['prix'].'<br>'  ;

  $res .= '<form id="image_item_ajout" method="POST" action="./ajouter">
            <input type="text" placeholder="<url de l\'image>" name="img_item_new">';

  $res.='<button type="submit" name="valider_image" value="valider_image1">Valider</button>
         </form>
         </section>';
  return $res;
}

private function afficher_formulaire_modif_image(){
  $res='<section>';
  $res.= 'Item numéro '.$this->tableau[0]['id'].'
          <br> Nom de l\'item = '.$this->tableau[0]['nom'].'
          <br> Description de l\'item = '.$this->tableau[0]['descr'].'
          <br> Participant : '.$this->tableau[0]['participant'].'<br>';

  $res .= '<form id="image_item_modif" method="POST" action="./modifier">
            <input type="text" placeholder="<url de l\'image>" name="img_item_modif">
            <button type="submit" name="valider_image" value="valider_image1">Valider la modification</button>
            <br><br>Image actuelle : <img src="'.$this->tableau[1]['url'].'"';

  $res.='
         </form>
         </section>';
  return $res;
}

<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> 242508db385339fe579acddf549f7b83ecb0b40e
private function afficher_formulaire_modif_item(){
  $res=$this->href_item_createur();
  $res.='<section>';

  $res.= 'Item numéro '.$this->tableau[0]['id'].'
          <br> Nom de l\'item = '.$this->tableau[0]['nom'].'
          <br> Description de l\'item = '.$this->tableau[0]['descr'].'
          <br> Participant : '.$this->tableau[0]['participant'].'<br>';

  $res .= '<form id="item_modif" method="POST" action="./../../listesCreateur">
            <input type="text" placeholder="<Nom>" name="nom_item_modif">
            <input type="text" placeholder="<Description>" name="descr_item_modif">
            <input type="number" placeholder="<Tarif>" name="tarif_item_modif">
            <button type="submit" name="valider_image" value="valider_image1">Valider la modification</button>';

  $res.='
         </form>
         </section>';
  return $res;
}
<<<<<<< HEAD
>>>>>>> 55d02053a278cdc0e5aefa68b4b8352ac37e7fdd
=======
>>>>>>> 242508db385339fe579acddf549f7b83ecb0b40e

public function render(int $selecteur) {
switch ($selecteur) {
 case 0 :
 $content = $this->afficherListeDesListes();
 break;

 case 1 :
 $content = $this->afficherItemsDeListe();
 break;

 case 2 :
 $content = $this->afficherItem();
 break;

 case 3 :
 $content = $this->afficher_formulaire_ajout_image();
 break;

 case 4 :
 $content = $this->afficher_formulaire_modif_image();
 break;

 case 5 :
 $content = $this->afficher_formulaire_modif_item();
 break;
}
$html = <<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>My Wishlist</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='CSS/projet.css'>
    </head>


<body>
    <div class="wrapper">
        <div class="header">
            <div class="nav">
                <div class="logo">
                    <strong>
                            <img src="CSS/logo.PNG" alt="My Wishlist"/>
                    </strong>
                </div>
                <div class="menu">
                    <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="authentification">Connexion</a></li>
                    <li><a href="inscription">Créer un compte</a></li>
						        <li><a href="listes">Afficher les listes</a></li>
                    <li><a href="listesCreateur">Afficher ses listes</a></li>
						        <li><a href="parametres">Paramètres de compte</a></li>
						        <li><a href="./">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content">
          $content
        </div>
    </div>
    <footer>
        <p>PROJET WEB / DAUBENFELD Gabriel - DENOIS Quentin - KRATZ Juliette / S3C</p>
    </footer>
</body><html>
END;
echo $html;
}

}
