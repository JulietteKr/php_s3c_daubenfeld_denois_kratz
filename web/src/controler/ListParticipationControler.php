<?php
namespace mywishlist\controler;

use \mywishlist\models\Liste;
use \mywishlist\models\Item;
use \mywishlist\models\Compte;
use \mywishlist\vue\VueCreateur;
use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueErreur;

class ListParticipationControler{

  public function listes(){
      echo "listes";
  }

  public function afficherListeDesListes(){
    $liste = Liste::get()->toArray();
    $vue = new VueParticipant($liste);
    $vue->render(0);
  }

  public function afficherItemsDeLaListe($idliste){
    $liste = \mywishlist\models\Liste::find($idliste)->toArray();
    $vue = new VueParticipant([$liste]);
    $vue->render(1);
  }

  public function afficherItem($id){
    $item = Item::find($id)->toArray();
    $vue = new VueParticipant([$item]);
    $vue->render(2);
  }

  public function reserver($id){
    $item = Item::find($id);
    $user = Compte::find($_SESSION['user_id']);
    $item->participant = $user->login;
    $item->message = $_POST['message'];
    $item->save();
    $this->afficherItem($id);
  }

  public function supprimerItemDeListe($id){
      $item = Item::find($id);
      if($item->participant != NULL){
          $item->participant = NULL;
          $item->save();
      }
      $this->afficherItemReserve();
  }

  public function afficherItemReserve(){
    $user = Compte::find($_SESSION['user_id']);
    $liste = Item::where("participant","=",$user->login)->get()->toArray();

    $vue = new VueParticipant($liste);
    $vue->render(2);

  }

  public function modifierListe($idliste){
    $liste = Liste::find($idliste);
    if($_SESSION['user_id']=$liste->user_id){
      if(isset($_POST['titreListe'])){
        $liste->titre = $_POST['titreListe'];
      }
      if(isset($_POST['descrListe'])){
        $liste->description = $_POST['descrListe'];
      }
      if(isset($_POST['expiration'])){
        $liste->expiration = $_POST['expiration'];
      }
      $liste->save();

      $liste = Liste::all()->toArray();
      $vue = new VueParticipant($liste);
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

  public function supprimerListe($idliste){
    $liste = Liste::find($idliste);
    if($_SESSION['user_id']=$liste->user_id){
      $liste->delete();

      $liste = Liste::all()->toArray();
      $vue = new VueParticipant($liste);
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

  public function afficherListesDuCreateur(){
    $liste = null;

    if( isset($_SESSION['user_id']) ){

      $liste = Liste::where('user_id','=',$_SESSION['user_id'])->get()->toArray();
      $vue = new VueCreateur($liste);
      $vue->render(0);
    }else{
      $vue = new VueErreur();
      $vue->render(0);
    }
  }

}
