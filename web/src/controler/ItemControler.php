<?php

namespace mywishlist\controler;


use \mywishlist\vue\VueCreateur;
use \mywishlist\vue\VueParticipant;
use \mywishlist\models\Liste;
use \mywishlist\models\Item;
use \mywishlist\models\Image;

class ItemControler{

public function afficherItems_Avec_Button_Image($idliste){
  $this->items_correspondant_idcreateur($idliste);
}

public function ajouter_item_listeCreateur($idliste){
  $item = new Item();

  $nom_item = $_POST['nom_item'];
  $descr_item = $_POST['description_item'];
  $prix_item = $_POST['prix_item'];

  $item->nom = filter_var($nom_item,FILTER_SANITIZE_STRING);
  $item->descr = filter_var($descr_item,FILTER_SANITIZE_STRING);

  if(filter_var($prix_item,FILTER_VALIDATE_FLOAT)){
      $item->tarif = $prix_item;
  }

  $item->liste_id = $idliste;
  $item->save();

  $this->items_correspondant_idcreateur($idliste);
}

public function supprimer_item_listeCrea($idliste){
  $item = Item::find($idliste);
  if($item->participant != NULL){
      $item->participant = NULL;
      $item->save();
  }
  $this->items_correspondant_idcreateur($idliste);
}

public function items_correspondant_idcreateur($idliste){
  $items = Item::where('liste_id','=',$idliste)->get()->toArray();
  $vue = new VueCreateur($items);
  $vue->render(1);
}

public function formulaire_modif_item($iditem){

  $item = Item::find($iditem)->first()->toArray();
  $res_tableau[] = $item;
  $vue =  new VueParticipant($res_tableau);
  $vue->render(5);

}

public function afficher_modif_item($iditem){

  $item = Item::find($iditem)->toArray();

  $item->nom = $_POST['nom_item_modif'];
  $item->descr = $_POST['descr_item_modif'];
  $item->tarif = $_POST['tarif_item_modif'];

  $res_tableau[] = $item;

  $item->save();

  $res_tableau[] = $item;

  $this->afficherItems_Avec_Button_Image();
}

}
