<?php

namespace mywishlist\controler;

use \mywishlist\vue\VueParticipant;
use \mywishlist\vue\VueImage;
use \mywishlist\models\Item;
use \mywishlist\models\Image;


class ImageControler{

  public function formulaire_ajout_image_on_item($iditem){

    $item_img = Item::find($iditem)->toArray();

    $image_verif_existence = Image::where("item_id","=",$iditem)->first();
    echo $image_verif_existence;
    // if($image_verif_existence->)
    $vue =  new VueParticipant($item_img);
    $vue->render(3);

  }

  public function afficher_image_ajout($iditem){

    $item_img = Item::find($iditem)->toArray();

    $res_tableau[] = $item_img;

    $img_url = $_POST['img_item_new'];
    $image = new Image();
    $image->url = $img_url;
    $image->item_id = $iditem;

    $image->save();

    $image->toArray();

    $res_tableau[] = $image;

    $vue =  new VueImage($res_tableau);
    $vue->render(0);
  }

  public function formulaire_modif_image_on_item($iditem){

    $item_img = Item::find($iditem)->first()->toArray();

    $image = Image::where("item_id","=",$iditem)->first()->toArray();
    $res_tableau[] = $item_img;
    $res_tableau[] = $image;
    // if($image_verif_existence->)
    $vue =  new VueParticipant($res_tableau);
    $vue->render(4);

  }

  public function afficher_image_modif($iditem){

    $item_img = Item::find($iditem)->toArray();

    $res_tableau[] = $item_img;

    $image = Image::where('item_id','=',$iditem)->first();
    $image->url = $_POST['img_item_modif'];
    $image->save();

    $image->toArray();

    $res_tableau[] = $image;

    $vue =  new VueImage($res_tableau);
    $vue->render(0);
  }

  public function formulaire_supprimer_image_on_item($iditem){

    $item_img = Item::find($iditem)->first()->toArray();

    $image = Image::where("item_id","=",$iditem)->first()->toArray();
    $res_tableau[] = $item_img;
    $res_tableau[] = $image;
    // if($image_verif_existence->)
    $vue =  new VueParticipant($res_tableau);
    $vue->render(4);

  }

  public function afficher_image_supprimer($iditem){


    // if (isset($_POST['supprimer_image_item'])){
      $item_img = Item::find($iditem)->toArray();

      $res_tableau[] = $item_img;

      $image = Image::where('item_id','=',$iditem)->first();
      $image->url = NULL;
      $image->item_id = NULL;

      $image->save();

      $image->toArray();

      $res_tableau[] = $image;

      $vue =  new VueImage($res_tableau);
      $vue->render(0);
    // }

  }








}
