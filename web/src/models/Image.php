<?php

namespace mywishlist\models;

class Image extends \Illuminate\Database\Eloquent\Model{

  protected $table = 'image';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function item(){
    return $this->belongsTo('\mywishlist\models\Item','item_id');
  }

}
